package com.ghtk.sample004.controllers;

import com.ghtk.sample004.requests.UserRequest;
import com.ghtk.sample004.requests.UserSearchRequest;
import com.ghtk.sample004.responses.DefaultResponse;
import com.ghtk.sample004.responses.UserResponse;
import com.ghtk.sample004.services.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
@Tag(name = "User", description = "User")
public class UserController {

  private final UserService userService;

  @GetMapping
  public ResponseEntity<DefaultResponse<List<UserResponse>>> list(
    @RequestParam(value = "page",required = false, defaultValue = "0") Integer page,
    @RequestParam(value = "limit", defaultValue = "100") Integer limit,
    @RequestParam(value = "username",required = false) String username,
    @RequestParam(value = "full_name", required = false) String fullName,
    @RequestParam(value = "gender", required = false) Integer gender
  ) {
    Pageable pageable = PageRequest.of(page, limit, Sort.by("id").descending());
    UserSearchRequest request = UserSearchRequest.builder()
      .fullName(fullName)
      .username(username)
      .gender(gender)
      .build();
    return ResponseEntity.ok(userService.list(pageable, request));
  }

//  Xử lý lỗi validation = tay
//  @PostMapping
//  @Operation(summary = "Tạo User")
//  public ResponseEntity<DefaultResponse<UserResponse>> create(
//    @Valid @RequestBody UserRequest request,
//    BindingResult bindingResult
//  ) {
//    if (bindingResult.hasErrors()) {
//      return ResponseEntity.badRequest()
//        .body(DefaultResponse.error(bindingResult.getAllErrors().get(0).getDefaultMessage()));
//    }
//    return ResponseEntity.ok(userService.create(request));
//  }

  @PostMapping
  @Operation(summary = "Tạo User")
  public ResponseEntity<DefaultResponse<UserResponse>> create(@Valid @RequestBody UserRequest request) {
    return ResponseEntity.ok(userService.create(request));
  }

}
