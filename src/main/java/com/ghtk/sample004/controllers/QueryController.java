package com.ghtk.sample004.controllers;

import com.ghtk.sample004.responses.DefaultResponse;
import com.ghtk.sample004.services.QueryService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/queries")
@RequiredArgsConstructor
@Tag(name = "Query", description = "Query")
public class QueryController {

  private final QueryService queryService;

  @GetMapping
  public ResponseEntity<DefaultResponse<Boolean>> getQueries() {
    queryService.showQuery();
//    queryService.fetchAllUser();
    return ResponseEntity.ok(DefaultResponse.success(Boolean.TRUE));
  }
}
