package com.ghtk.sample004.controllers;

import com.ghtk.sample004.responses.DefaultResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
public class ExceptionController {

  @ExceptionHandler(BindException.class)
  public ResponseEntity<DefaultResponse<Object>> handleBindException(BindException e) {
    if (e.hasErrors()) {
      return ResponseEntity.badRequest()
        .body(DefaultResponse.error(e.getAllErrors().get(0).getDefaultMessage()));
    }
    return ResponseEntity.badRequest()
      .body(DefaultResponse.error("Request không hợp lệ!"));
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity<DefaultResponse<Object>> handleException(Exception e) {
    log.error("Có lỗi xảy ra: ", e);
    return ResponseEntity.ok(DefaultResponse.error("Có lỗi xảy ra. Vui lòng liên hệ Admin."));
  }

}
