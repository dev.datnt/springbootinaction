package com.ghtk.sample004.repositories;

import com.ghtk.sample004.entities.User;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.domain.Pageable;

public interface UserRepositoryCustom {

  List<User> searchTopUser(
      Integer gender, Integer status, LocalDateTime createdAt, Pageable pageable);

  List<User> searchTopUserWithNativeQuery(
      Integer gender, Integer status, LocalDateTime createdAt, Pageable pageable);
}
