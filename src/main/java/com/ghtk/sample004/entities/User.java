package com.ghtk.sample004.entities;

import com.ghtk.sample004.requests.UserRequest;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
public class User {

  public static final int STATUS_INACTIVE = 0;
  public static final int STATUS_ACTIVE = 1;

  public static final int GENDER_FEMALE = 0;
  public static final int GENDER_MALE = 1;

  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "full_name")
  private String fullName;

  @Column(name = "gender")
  private Integer gender;

  @Column(name = "username")
  private String username;

  @Column(name = "status")
  private Integer status;

  @Column(name = "role_id")
  private Long roleId;

  @Column(name = "daily_wage")
  private BigDecimal dailyWage;

  @Column(name = "created_by")
  private Long createdBy;

  @Column(name = "created_at")
  private LocalDateTime createdAt;

  @Column(name = "modified_by")
  private Long modifiedBy;

  @Column(name = "modified_at")
  private LocalDateTime modifiedAt;

  public static User of(UserRequest request) {
    return User.builder()
        .fullName(request.getFullName())
        .gender(request.getGender())
        .username(request.getUsername())
        .status(request.getStatus())
        .roleId(request.getRoleId())
        .dailyWage(request.getDailyWage())
        .build();
  }

  @Override
  public String toString() {
    return "User{"
        + "id="
        + id
        + ", fullName='"
        + fullName
        + '\''
        + ", username='"
        + username
        + '\''
        + '}';
  }
}
