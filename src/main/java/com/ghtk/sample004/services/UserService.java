package com.ghtk.sample004.services;

import com.ghtk.sample004.requests.UserRequest;
import com.ghtk.sample004.requests.UserSearchRequest;
import com.ghtk.sample004.responses.DefaultResponse;
import com.ghtk.sample004.responses.UserResponse;
import java.util.List;
import org.springframework.data.domain.Pageable;

public interface UserService {

  DefaultResponse<List<UserResponse>> list(Pageable pageable, UserSearchRequest request);

  DefaultResponse<UserResponse> create(UserRequest request);

}
