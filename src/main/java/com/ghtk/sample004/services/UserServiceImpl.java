package com.ghtk.sample004.services;

import com.ghtk.sample004.entities.User;
import com.ghtk.sample004.repositories.UserRepository;
import com.ghtk.sample004.requests.UserRequest;
import com.ghtk.sample004.requests.UserSearchRequest;
import com.ghtk.sample004.responses.DefaultResponse;
import com.ghtk.sample004.responses.UserResponse;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

  private final UserRepository userRepository;

  @Override
  public DefaultResponse<List<UserResponse>> list(Pageable pageable, UserSearchRequest request) {
    List<Specification<User>> specifications = new ArrayList<>();
    if (StringUtils.hasText(request.getUsername())) {
      specifications.add(
        (root, query, builder) -> builder.like(root.get("username"), request.getUsername().strip() + "%"));
    }
    if (StringUtils.hasText(request.getFullName())) {
      specifications.add(
        (root, query, builder) -> builder.like(root.get("fullName"), request.getFullName().strip() + "%"));
    }
    if (Objects.nonNull(request.getGender())) {
      specifications.add((root, query, builder) -> builder.equal(root.get("gender"), request.getGender()));
    }
    Page<User> page = userRepository.findAll(Specification.allOf(specifications), pageable);
    return DefaultResponse.success(
      page,
      page.hasContent() ? page.get().map(UserResponse::of).toList() : Collections.emptyList());
  }
  @Override
  @Transactional(rollbackFor = Throwable.class, noRollbackFor = Exception.class)
  public DefaultResponse<UserResponse> create(UserRequest request) {
    User user = User.of(request);
    user.setCreatedBy(0L);
    user.setCreatedAt(LocalDateTime.now());
    user = userRepository.save(user);
    return DefaultResponse.success(UserResponse.of(user));
  }

}
