package com.ghtk.sample004.exceptions;

public class ERPRuntimeException extends RuntimeException {

  public ERPRuntimeException(String message) {
    super(message);
  }

}
