package com.ghtk.sample004.requests;

import com.fasterxml.jackson.databind.PropertyNamingStrategies.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import java.math.BigDecimal;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonNaming(SnakeCaseStrategy.class)
public class UserRequest {

  @Schema(description = "id")
  private Long id;
  @NotEmpty(message = "Họ và tên bắt buộc nhập!")
  @Schema(description = "Họ và tên")
  private String fullName;
  @Schema(description = "Giới tính")
  private Integer gender;
  @Schema(description = "Username")
  private String username;
  @Schema(description = "Trạng thái")
  private Integer status;
  @Schema(description = "ID của Role")
  private Long roleId;
  @Schema(description = "Lương 1 ngày")
  private BigDecimal dailyWage;

}
