package com.ghtk.sample004.responses;

import com.fasterxml.jackson.databind.PropertyNamingStrategies.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.ghtk.sample004.entities.User;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(SnakeCaseStrategy.class)
public class UserResponse {

  private Long id;
  private String fullName;
  private Integer gender;
  private String username;
  private Integer status;
  private Long roleId;
  private BigDecimal dailyWage;
  private Long createdBy;
  private LocalDateTime createdAt;
  private Long modifiedBy;
  private LocalDateTime modifiedAt;

  public static UserResponse of(User user) {
    return UserResponse.builder()
      .id(user.getId())
      .fullName(user.getFullName())
      .gender(user.getGender())
      .username(user.getUsername())
      .status(user.getStatus())
      .roleId(user.getRoleId())
      .dailyWage(user.getDailyWage())
      .createdBy(user.getCreatedBy())
      .createdAt(user.getCreatedAt())
      .build();
  }

}
